﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoundersOfBlueport.Data.Enums
{
    public enum ColorEnum
    {
        Blue,
        Brown,
        Green,
        Orange,
        Red,
        White
        
    }
}
