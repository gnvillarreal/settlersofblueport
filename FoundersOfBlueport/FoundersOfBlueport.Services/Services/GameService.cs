﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FoundersOfBlueport.Data.Models;

namespace FoundersOfBlueport.Services.Services
{
    public class GameService
    {
        public bool IsPlayerValid(Player player)
        {
            bool result = !String.IsNullOrWhiteSpace(player.Name) && player.Name.Length <= 20;
            return result;
        }
    }
}