﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(FoundersOfBlueport.Startup))]
namespace FoundersOfBlueport
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
